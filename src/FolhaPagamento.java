
public class FolhaPagamento {
	
	
	private double horasTrabalhadas;
	private double valorHoras;
	private int depententes;
	
	public FolhaPagamento( double horasTrabalhadas, double valorHoras, int dependentes ) {
		this.setHorasTrabalhadas(horasTrabalhadas);
		this.setValorHoras(valorHoras);
		this.setDepententes(dependentes);
	}
	
	public double salarioLiquido() {
		return calculaSalarioBruto() - descontoINSS() - descontoIR();
	}

	public int getDepententes() {
		return depententes;
	}

	public void setDepententes(int depententes) {
		this.depententes = depententes;
	}

	public double getValorHoras() {
		return valorHoras;
	}

	public void setValorHoras(double valorHoras) {
		this.valorHoras = valorHoras;
	}

	public double getHorasTrabalhadas() {
		return horasTrabalhadas;
	}

	public void setHorasTrabalhadas(double horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}
	
	public double calculaSalarioBruto(){
		return (getHorasTrabalhadas() * getValorHoras()) + (50 * getDepententes());
	}
	
	public double descontoINSS() {
		if( calculaSalarioBruto() <= 1000 ) {
			return calculaSalarioBruto() * 8.5/100;
		}else if( calculaSalarioBruto() > 1000 ) {
			return calculaSalarioBruto() * 9/100;
		}else {
			return 0;
		}
	}
	
	public double descontoIR() {
		if( calculaSalarioBruto() > 500 && calculaSalarioBruto() <= 1000 ) {
			return calculaSalarioBruto() * 5/100; 
		}else if( calculaSalarioBruto() > 1000 ) {
			return  calculaSalarioBruto() * 7/100; 
		}else {
			return 0;
		}
	}
	
	
	
}
