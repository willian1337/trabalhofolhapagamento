
public class TesteFolhaPagamento {
	public static void main(String[] args) {
		FolhaPagamento folha = new FolhaPagamento( 40, 150.0, 3 );
		
		System.out.println("Sal�rio Bruto: " + folha.calculaSalarioBruto() );
		System.out.println("Desconto INSS: " + folha.descontoINSS() );
		System.out.println("Imposto de Renda: " + folha.descontoIR() );
		System.out.println("Sal�rio Liquido: " + folha.salarioLiquido() );
	}
}
